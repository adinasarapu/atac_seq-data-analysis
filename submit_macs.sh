#!/bin/sh

# https://github.com/ParkerLab/ATACseq-Snakemake
# https://www.biostars.org/p/319809/

# SBATCH --job-name=MACS.ATAC

# job submit as
# sbatch --cpus-per-task=12 macs.sh

# scontrol show job 384
# scancel 384
# sinfo
# Interactive job
# srun --pty bash

PROJ_DIR=$HOME/ATAC
OUT_DIR=$PROJ_DIR/MACS_BAMPE

if [ ! -d ${OUT_DIR} ]; then
  mkdir -p ${OUT_DIR}
fi

FILE_CTRL=`ls -la ~/ATAC/BWA/NG01-ATAQ-Seq-DNA-118*/NG01-ATAQ-Seq-DNA-*.dedupe.bam | awk '{print $9}'`
FILE_TRT=`ls -la ~/ATAC/BWA/NG01-ATAQ-Seq-DNA-119*/NG01-ATAQ-Seq-DNA-*.dedupe.bam | awk '{print $9}'`

#  "--f BAMPE " or "--nomodel --f BAM --shift -100 --extsize 200"
# DEFAULT: 0.05. -q, and -p are mutually exclusive.

# If you followed original protocol for ATAC-Seq, you should get Paired-End reads. 
# If so, I would suggest you just use "--format BAMPE" to let MACS2 pileup the whole fragments in general. 
# But if you want to focus on looking for where the 'cutting sites' are, then '--nomodel --shift -100 --extsize 200' should work.

# With -f BAMPE on, MACS2 read the left mate and the insertion length information from BAM file, and discard right mate. With -f BAM, MACS2 only keeps the left mate, so it's definitely not a feature you want for your paired-end data.

macs2 callpeak \
 -n bam_pe \
 --format BAMPE \
 -g mm -q 0.01 --keep-dup all \
 --control $FILE_CTRL \
 --treatment $FILE_TRT \
 -B --trackline --SPMR \
 --call-summits \
 --outdir $OUT_DIR

#--nomodel --f BAM --shift -100 --extsize 200
