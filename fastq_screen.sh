#!/bin/sh

#SBATCH --job-name=ATAC_QC

# job submit as
# sbatch --cpus-per-task=13 bcl2fastq_job2.sh

#scontrol show job 384
#scancel 384
#sinfo

# Download genomes from iGenomes 
# https://support.illumina.com/sequencing/sequencing_software/igenome.html

# Edit .conf file (/home/adinasarapu/fastq_screen_v0.13.0/fastq_screen.conf )
# THREADS         12
# DATABASE        Human   /sw/eicc/Data/Illumina/Homo_sapiens/UCSC/hg38/Sequence/Bowtie2Index/genome
# DATABASE        Mouse   /sw/eicc/Data/Illumina/Mus_musculus/UCSC/mm10/Sequence/Bowtie2Index/genome
# ...

PROJ_DIR=$HOME/NG01_ATAC
SEQ_DATA_DIR=$PROJ_DIR/FASTQ
OUT_DIR=$PROJ_DIR/FASTQC

# QC sub-directory will be created 
if [ ! -d $OUT_DIR ]; then
 mkdir -p $PROJ_DIR/FASTQC
 mkdir -p $PROJ_DIR/FASTQ_SCREEN
 mkdir -p $PROJ_DIR/FASTQC_MULTIQC
fi

# NG01-ATAQ-Seq-DNA-1187_S1_R1_001.fastq.gz  NG01-ATAQ-Seq-DNA-1189_S3_R1_001.fastq.gz  NG01-ATAQ-Seq-DNA-1191_S5_R1_001.fastq.gz
# NG01-ATAQ-Seq-DNA-1187_S1_R2_001.fastq.gz  NG01-ATAQ-Seq-DNA-1189_S3_R2_001.fastq.gz  NG01-ATAQ-Seq-DNA-1191_S5_R2_001.fastq.gz
# NG01-ATAQ-Seq-DNA-1188_S2_R1_001.fastq.gz  NG01-ATAQ-Seq-DNA-1190_S4_R1_001.fastq.gz  NG01-ATAQ-Seq-DNA-1192_S6_R1_001.fastq.gz
# NG01-ATAQ-Seq-DNA-1188_S2_R2_001.fastq.gz  NG01-ATAQ-Seq-DNA-1190_S4_R2_001.fastq.gz  NG01-ATAQ-Seq-DNA-1192_S6_R2_001.fastq.gz

module load bowtie2/2.2.6

# get sample IDs from file names
list=$(ls $SEQ_DATA_DIR | xargs -n1 basename | awk 'BEGIN{FS="_001"}{ print $1 }' | sort | uniq)

echo $list

for file in $list; do

# 1. Fastq_Screen
fastq_screen \
 --aligner bowtie2 \
 -conf /home/adinasarapu/fastq_screen_v0.13.0/fastq_screen.conf \
 --subset 100000 \
 --outdir $PROJ_DIR/FASTQ_SCREEN \
 $SEQ_DATA_DIR/${file}_001.fastq.gz

done

module unload bowtie2/2.2.6

# 2. FastQC
module load FastQC/0.11.4
fastqc -t 10 $SEQ_DATA_DIR/*.fastq.gz -o $PROJ_DIR/FASTQC
module unload FastQC/0.11.4

# 3. MultiQC
multiqc $OUT_DIR --outdir $PROJ_DIR/FASTQC_MULTIQC

