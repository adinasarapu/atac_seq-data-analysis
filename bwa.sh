#!/bin/sh

##There are three alignment algorithms in BWA: `mem', `bwasw', and 
## `aln/samse/sampe'. If you are not sure which to use, try `bwa mem' first.
## bwa (alignment via Burrows-Wheeler transformation)
## mem (BWA-MEM algorithm)

##SBATCH --job-name=BWA_1187

# job submit as
## sbatch --cpus-per-task=12 bwa.sh

#scontrol show job 384
#scancel 384
#sinfo

module load bwa/0.7.12
module load samtools/1.3
module load picardtools/2.0.1

echo "bwa mem -t $THREADS $REF_DIR/genome.fa $R1 $R2 > $OUT_DIR/${SID}.sam"
bwa mem -t $THREADS $REF_DIR/genome.fa $R1 $R2 > ${OUT_DIR}/${SID}.sam

sed '/chrM/d;/random/d;/chrUn/d' < $OUT_DIR/${SID}.sam > $OUT_DIR/${SID}_filtered.sam
samtools view -bS $OUT_DIR/${SID}_filtered.sam > $OUT_DIR/${SID}.bam

# Remove the mitochondrial reads after alignment. 
# A python script, creatively named removeChrom, is available in the ATAC-seq module to accomplish this. 
# For example, to remove all 'chrM' reads from a BAM file, one would run this:
# samtools view -h $OUT_DIR/${SID}.bam | eval "$(python $PROJ_DIR/scripts/removeChrom.py - - chrM)" | samtools view -b - > $OUT_DIR/${SID}.noMT.bam

samtools sort -T /tmp/${SID}.sorted -o $OUT_DIR/${SID}.sorted.bam $OUT_DIR/${SID}.bam

#  MarkDuplicates looks at the starting position and the CIGAR string of the reads.
#  If the start position and CIGAR are identical, then the reads are
#  duplicates. So the program only leaves one read untouched and marks the
#  others so they can be ignored in downstream analyses.
# after loading picartools
# echo $PICARD
# java -Xmx2g -jar /sw/eicc/Pkgs/picardtools/2.0.1/picard.jar
$PICARD MarkDuplicates \
	MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 \
	METRICS_FILE=$OUT_DIR/${SID}.metrics \
	REMOVE_DUPLICATES=true \
	ASSUME_SORTED=true \
	VALIDATION_STRINGENCY=LENIENT \
	INPUT=$OUT_DIR/${SID}.sorted.bam \
	OUTPUT=$OUT_DIR/${SID}.dedupe.bam

samtools index $OUT_DIR/${SID}.dedupe.bam

module unload picardtools/2.0.1
module unload bwa/0.7.12
module unload samtools/1.3
