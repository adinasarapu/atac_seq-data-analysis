#!/bin/sh

#################################################
# Ashok R. Dinasarapu Ph.D                      #
# Emory University, Dept. of Human Genetics     #
# ashok.reddy.dinasarapu@emory.edu              #
# Last updated on 034/29/2019                    #
#################################################

PROJ_DIR=$HOME/ATAC
SCRIPT=$PROJ_DIR/scripts/bwa.sh
FASTQ_DIR=$PROJ_DIR/FASTQ

REF_DIR=$HOME/iGenomes/Mus_musculus/UCSC/mm10/Sequence/BWAIndex

THREADS=12

list=$(ls $FASTQ_DIR/*.fastq.gz | xargs -n1 basename | awk 'BEGIN{FS="_R"}{ print $1 }' | sort | uniq)

echo $list

for file in $list; do

echo "Job submitted for sample ${file} ..."
R1=$FASTQ_DIR/${file}_R1_001.fastq.gz
R2=$FASTQ_DIR/${file}_R2_001.fastq.gz
echo $R1 " ---> " $R2

OUT_DIR=$PROJ_DIR/BWA/$file
if [ ! -d $OUT_DIR ]; then
  mkdir -p $OUT_DIR
fi

sbatch \
 --job-name=$file \
 --mail-type=END --mail-user=ashok.reddy.dinasarapu@emory.edu \
 --cpus-per-task=$THREADS \
 --export=PROJ_DIR=$PROJ_DIR,R1=$R1,R2=$R2,THREADS=$THREADS,REF_DIR=$REF_DIR,OUT_DIR=$OUT_DIR,SID=$file \
 $SCRIPT

sleep 2

done
