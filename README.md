# ATAC_seq data analysis  
ATAC-seq (Assay for Transposase Accessible Chromatin with high-throughput Sequencing) is a next-generation sequencing approach for the analysis of open chromatin regions to assess genome-wise chromatin accessibility.  

## STEP 1: Read QC (uisng FastQC, FastQ_Screen and MultiQC)  
## STEP 2: Read Mapping (using BWA-mem aligner)  
## STEP 3: Peak calling (using MACS2)
